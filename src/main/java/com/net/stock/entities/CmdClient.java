package com.net.stock.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CmdClient implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idCmdCl;
	
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date datecmd;
	
	@ManyToOne
	@JoinColumn(name="idcl")
	private Clients clients;
	
	@OneToMany(mappedBy="cmdCL")
	private List<LignCmdClient> LignCmdCl;

	public Long getIdCmdCl() {
		return idCmdCl;
	}

	public void setIdCmdCl(Long idCmdCl) {
		this.idCmdCl = idCmdCl;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDatecmd() {
		return datecmd;
	}

	public void setDatecmd(Date datecmd) {
		this.datecmd = datecmd;
	}

	public Clients getClients() {
		return clients;
	}

	public void setClients(Clients clients) {
		this.clients = clients;
	}

	public List<LignCmdClient> getLignCmdCl() {
		return LignCmdCl;
	}

	public void setLignCmdCl(List<LignCmdClient> lignCmdCl) {
		LignCmdCl = lignCmdCl;
	}

	public CmdClient(String code, Date datecmd) {
		super();
		this.code = code;
		this.datecmd = datecmd;
	}

	public CmdClient() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
