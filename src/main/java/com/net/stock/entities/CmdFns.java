package com.net.stock.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CmdFns implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idCmdfrns;
	
	private String codecmdFr;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datecmdfrn;
	
	
	@OneToMany(mappedBy="frns")
	private List<LignCmdFrns>LignCmdFr;
	
	@ManyToOne
	@JoinColumn(name="idfrns")
	private Fournisseur frnse;

	public Long getIdCmdfrns() {
		return idCmdfrns;
	}

	public void setIdCmdfrns(Long idCmdfrns) {
		this.idCmdfrns = idCmdfrns;
	}

	public String getCodecmdFr() {
		return codecmdFr;
	}

	public void setCodecmdFr(String codecmdFr) {
		this.codecmdFr = codecmdFr;
	}

	public Date getDatecmdfrn() {
		return datecmdfrn;
	}

	public void setDatecmdfrn(Date datecmdfrn) {
		this.datecmdfrn = datecmdfrn;
	}

	public List<LignCmdFrns> getLignCmdFr() {
		return LignCmdFr;
	}

	public void setLignCmdFr(List<LignCmdFrns> lignCmdFr) {
		LignCmdFr = lignCmdFr;
	}

	public Fournisseur getFrnse() {
		return frnse;
	}

	public void setFrnse(Fournisseur frnse) {
		this.frnse = frnse;
	}

	public CmdFns(String codecmdFr, Date datecmdfrn) {
		super();
		this.codecmdFr = codecmdFr;
		this.datecmdfrn = datecmdfrn;
	}

	public CmdFns() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
