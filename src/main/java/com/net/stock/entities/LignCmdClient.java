package com.net.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LignCmdClient implements Serializable{
	
	
	@Id
	@GeneratedValue
	private Long idLingncldCl;
	private String code;
	
	@ManyToOne
	@JoinColumn(name="idarticles")
	private Articles articles;

	@ManyToOne
	@JoinColumn(name="idcmdcl")
	private CmdClient cmdCL;
	

public Long getIdLingncldCl() {
	return idLingncldCl;
}

public void setIdLingncldCl(Long idLingncldCl) {
	this.idLingncldCl = idLingncldCl;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public Articles getArticles() {
	return articles;
}

public void setArticles(Articles articles) {
	this.articles = articles;
}

public CmdClient getCmdCL() {
	return cmdCL;
}

public void setCmdCL(CmdClient cmdCL) {
	this.cmdCL = cmdCL;
}

public LignCmdClient() {
	super();
}


}
