package com.net.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity

public class Articles implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idArticle;
	
	private String CodeArticle;
	private String Designation;
	private String photo;
	private BigDecimal pu,tva,ttc;
	
	@ManyToOne
	@JoinColumn(name = "IDCAT")
	private Categories categories;
	
	@OneToMany(mappedBy = "articles")
	private List<LignCmdClient>lignCmdCl;
	
	@OneToMany(mappedBy="article")
	private List<LignVente>lignVente;
	
	
	
	
	public List<LignVente> getLignVente() {
		return lignVente;
	}
	public void setLignVente(List<LignVente> lignVente) {
		this.lignVente = lignVente;
	}
	public Long getIdArticle() {
		return idArticle;
	}
	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
	public String getCodeArticle() {
		return CodeArticle;
	}
	
	public List<LignCmdClient> getLignCmdCl() {
		return lignCmdCl;
	}
	public void setLignCmdCl(List<LignCmdClient> lignCmdCl) {
		this.lignCmdCl = lignCmdCl;
	}
	public void setCodeArticle(String codeArticle) {
		CodeArticle = codeArticle;
	}
	public String getDesignation() {
		return Designation;
	}
	public void setDesignation(String designation) {
		Designation = designation;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public BigDecimal getPu() {
		return pu;
	}
	public void setPu(BigDecimal pu) {
		this.pu = pu;
	}
	public BigDecimal getTva() {
		return tva;
	}
	public void setTva(BigDecimal tva) {
		this.tva = tva;
	}
	public BigDecimal getTtc() {
		return ttc;
	}
	public void setTtc(BigDecimal ttc) {
		this.ttc = ttc;
	}
	public Categories getCategories() {
		return categories;
	}
	public void setCategories(Categories categories) {
		this.categories = categories;
	}
	
	public Articles() {
		super();
	}
	public Articles(String codeArticle, String designation, String photo, BigDecimal pu, BigDecimal tva,
			BigDecimal ttc) {
		super();
		CodeArticle = codeArticle;
		Designation = designation;
		this.photo = photo;
		this.pu = pu;
		this.tva = tva;
		this.ttc = ttc;
	}

	
	
	

}
