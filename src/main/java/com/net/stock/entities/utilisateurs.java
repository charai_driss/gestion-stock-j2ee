package com.net.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class utilisateurs implements Serializable{

	
	@Id
	@GeneratedValue
	private Long idusers;
	
	private String login;
	
	private String email;
	private String password;
	private String photo;
	public Long getIdusers() {
		return idusers;
	}
	public void setIdusers(Long idusers) {
		this.idusers = idusers;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public utilisateurs(String login, String email, String password, String photo) {
		super();
		this.login = login;
		this.email = email;
		this.password = password;
		this.photo = photo;
	}
	public utilisateurs() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
}
