package com.net.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvmtStock implements Serializable {
	
	public static final int ENTREE=1;
	public static final int SORTIE=2;
	@Id
	@GeneratedValue
	private Long idMvmt;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvmt;
	
	private BigDecimal qte;
	
	private int typeMvmt;
	@ManyToOne
	@JoinColumn(name="idarticle")
	private Articles article;
	public Long getIdMvmt() {
		return idMvmt;
	}
	public void setIdMvmt(Long idMvmt) {
		this.idMvmt = idMvmt;
	}
	public Date getDateMvmt() {
		return dateMvmt;
	}
	public void setDateMvmt(Date dateMvmt) {
		this.dateMvmt = dateMvmt;
	}
	public BigDecimal getQte() {
		return qte;
	}
	public void setQte(BigDecimal qte) {
		this.qte = qte;
	}
	public int getTypeMvmt() {
		return typeMvmt;
	}
	public void setTypeMvmt(int typeMvmt) {
		this.typeMvmt = typeMvmt;
	}
	public Articles getArticle() {
		return article;
	}
	public void setArticle(Articles article) {
		this.article = article;
	}
	public MvmtStock(Date dateMvmt, BigDecimal qte, int typeMvmt) {
		super();
		this.dateMvmt = dateMvmt;
		this.qte = qte;
		this.typeMvmt = typeMvmt;
	}
	public MvmtStock() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
