package com.net.stock.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Categories implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCategories;
	
	private String CodeCat;
	
	private String Descrip;
	
	@OneToMany(mappedBy= "categories" )
	private List<Articles> articles;

	public Long getIdCategories() {
		return idCategories;
	}

	public void setIdCategories(Long idCategories) {
		this.idCategories = idCategories;
	}

	public String getCodeCat() {
		return CodeCat;
	}

	public void setCodeCat(String codeCat) {
		CodeCat = codeCat;
	}

	public String getDescrip() {
		return Descrip;
	}

	public void setDescrip(String descrip) {
		Descrip = descrip;
	}

	public List<Articles> getArticles() {
		return articles;
	}

	public void setArticles(List<Articles> articles) {
		this.articles = articles;
	}

	public Categories(String codeCat, String descrip) {
		super();
		CodeCat = codeCat;
		Descrip = descrip;
	}

	public Categories() {
		super();
	} 
	
	
	
}
