package com.net.stock.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable {

	@Id
	@GeneratedValue
	private Long idvente;
	
	private String codeVente;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	
	@OneToMany(mappedBy="vente")
	private List<LignVente>lignVente;

	public Long getIdvente() {
		return idvente;
	}

	public void setIdvente(Long idvente) {
		this.idvente = idvente;
	}

	public String getCodeVente() {
		return codeVente;
	}

	public void setCodeVente(String codeVente) {
		this.codeVente = codeVente;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LignVente> getLignVente() {
		return lignVente;
	}

	public void setLignVente(List<LignVente> lignVente) {
		this.lignVente = lignVente;
	}

	public Vente(String codeVente, Date dateVente) {
		super();
		this.codeVente = codeVente;
		this.dateVente = dateVente;
	}

	public Vente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
