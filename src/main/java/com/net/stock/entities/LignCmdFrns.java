package com.net.stock.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LignCmdFrns implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLingncldfns;
	
	private String codeligncmd;
	
	
	@ManyToOne
	@JoinColumn(name="idfrns")
	private Fournisseur frns;
	
	@ManyToOne
	@JoinColumn(name="idcmdfrns")
	private CmdFns cmdFrns;

	public Long getIdLingncldfns() {
		return idLingncldfns;
	}

	public void setIdLingncldfns(Long idLingncldfns) {
		this.idLingncldfns = idLingncldfns;
	}

	public String getCodeligncmd() {
		return codeligncmd;
	}

	public void setCodeligncmd(String codeligncmd) {
		this.codeligncmd = codeligncmd;
	}

	public Fournisseur getFrns() {
		return frns;
	}

	public void setFrns(Fournisseur frns) {
		this.frns = frns;
	}

	public CmdFns getCmdFrns() {
		return cmdFrns;
	}

	public void setCmdFrns(CmdFns cmdFrns) {
		this.cmdFrns = cmdFrns;
	}

	public LignCmdFrns() {
		super();
	}
	
	

}
