package com.net.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Clients implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCl;
	private String nom, prenom,adresse,email,photo;
	
	@OneToMany(mappedBy = "clients")
	private List<CmdClient> cmdClient;

	public Long getIdCl() {
		return idCl;
	}

	public void setIdCl(Long idCl) {
		this.idCl = idCl;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<CmdClient> getCmdClient() {
		return cmdClient;
	}

	public void setCmdClient(List<CmdClient> cmdClient) {
		this.cmdClient = cmdClient;
	}

	public Clients(String nom, String prenom, String adresse, String email, String photo) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.email = email;
		this.photo = photo;
	}

	public Clients() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
