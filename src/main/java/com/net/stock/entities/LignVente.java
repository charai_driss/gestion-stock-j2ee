package com.net.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.param.VersionTypeSeedParameterSpecification;

@Entity
public class LignVente implements Serializable{
	
	
	@Id
	@GeneratedValue
	private Long idLignVnete;
	
	@ManyToOne
	@JoinColumn(name="idarticle")
	private Articles article;
	
	@ManyToOne
	@JoinColumn(name="idvente")
	private Vente vente;

	public Long getIdLignVnete() {
		return idLignVnete;
	}

	public void setIdLignVnete(Long idLignVnete) {
		this.idLignVnete = idLignVnete;
	}

	public Articles getArticle() {
		return article;
	}

	public void setArticle(Articles article) {
		this.article = article;
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

	public LignVente() {
		super();
	}
	
	
	

}
